import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllGamesComponent } from './component/all-games/all-games.component';



const routes: Routes = [
  { path: '', redirectTo: 'all-games', pathMatch: 'full' },
  { path: 'all-games', component: AllGamesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
