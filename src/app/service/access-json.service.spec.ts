import { TestBed } from '@angular/core/testing';

import { AccessJsonService } from './access-json.service';

describe('AccessJsonService', () => {
  let service: AccessJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccessJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
