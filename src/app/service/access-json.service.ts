import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccessJsonService {

  constructor(private http: HttpClient) {
 
  }

  public getTechJSON(): Observable<any> {
    return this.http.get("./assets/api/technology.json");
  }

  public getTeamJSON(): Observable<any> {
    return this.http.get("./assets/api/teams.json");
  }

  public getTechnologyStackJson(): Observable<any> {
    return this.http.get("./assets/api/technologyStack.json");
  }
}
